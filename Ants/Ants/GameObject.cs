﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Ants
{
    class GameObject : IPhysicsObject
    {
        public UIElement Render { get; protected set; }
        public CoOrdinate Location;
        private double speed;
        private double throttle = 0;
        public double Throttle { get => throttle; set { throttle = value > 0 && value <= 1 ? value : throttle; } }
        public Vector direction { get; protected set; }
        protected static readonly Vector defaultDirection = new Vector(1, 2);

        public Relm parentRelm { get; protected set; }
        public Relm currentRelm;
        public int CurrentRelmID => currentRelm.ID;
        public int ParentRelmID => parentRelm.ID;

        public GameObject(double speed, Relm parent)
        {
            this.speed = speed;
            parentRelm = parent;
            currentRelm = parent;
            Render = new Ellipse() { Width = 14, Height = 14, Fill = Brushes.Orange, Margin = new Thickness(-7) };
            Location = new CoOrdinate(Relm.Size / 2, Relm.Size / 2);
            direction = defaultDirection;
        }

        public GameObject(double speed, Relm parent, CoOrdinate location)
        {
            this.speed = speed;
            parentRelm = parent;
            currentRelm = parent;
            Render = new Ellipse() { Width = 14, Height = 14, Fill = Brushes.Orange, Margin = new Thickness(-7) };
            Location = location;
            direction = defaultDirection;
        }

        public virtual void Update(FrameType type)
        {
            // do stuff to change location
            if (type != FrameType.spawn)
            {
                Location.X += speed * throttle * direction.X;
                Location.Y += speed * throttle * direction.Y;
            }

            // render change
            if (type != FrameType.update)
            {
                Canvas.SetLeft(Render, Location.X);
                Canvas.SetTop(Render, Location.Y);
            }
        }

        public void Reset()
        {
            Location = Relm.centre.Copy();
            parentRelm.AddGameObject(Relm.TravelDirection.Centre, this);
        }
    }

    public enum FrameType
    {
        animation,
        update,
        spawn
    }

    public sealed class CoOrdinate
    {
        public double X, Y;

        public double Hs { get { return Math.Pow(X, 2) + Math.Pow(Y, 2); } }
        public double H { get { return Math.Sqrt(Hs); } }

        public CoOrdinate() { }
        public CoOrdinate(double X, double Y)
        {
            this.X = X;
            this.Y = Y;
        }

        public static bool operator >(CoOrdinate a, CoOrdinate b)
        {
            return a.Hs > b.Hs;
        }
        public static bool operator <(CoOrdinate a, CoOrdinate b)
        {
            return a.Hs < b.Hs;
        }
        public static CoOrdinate operator +(CoOrdinate a, CoOrdinate b)
        {
            if (a == null || b == null)
                return new CoOrdinate(0, 0);
            else
                return new CoOrdinate(a.X + b.X, a.Y + b.Y);
        }
        public static CoOrdinate operator -(CoOrdinate a, CoOrdinate b)
        {
            if (a == null || b == null)
                return new CoOrdinate(0, 0);
            else
                return new CoOrdinate(a.X - b.X, a.Y - b.Y);
        }
        public override string ToString()
        {
            return $"X:{X},Y:{Y}";
        }
        public CoOrdinate Copy()
        {
            return new CoOrdinate(X, Y);
        }
    }

    public sealed class Vector
    {
        public double X { get; private set; }
        public double Y { get; private set; }

        public void Init(double X, double Y)
        {
            double a = 1 / (Math.Abs(X) + Math.Abs(Y));
            X *= a;
            Y *= a;
            this.Y = double.IsNaN(Y) ? 0 : Y;
            this.X = double.IsNaN(X) ? 0 : X;
        }

        public Vector(double X, double Y)
        {
            Init(X, Y);
        }

        public Vector(Size size)
        {
            Init(size.Width, size.Height);
        }

        public Vector(CoOrdinate coOrdinate)
        {
            Init(coOrdinate.X, coOrdinate.Y);
        }

        public Vector(double angle)
        {
            angle = angle > FullRadions ? angle - FullRadions : angle;
            angle = angle < 0 ? angle + FullRadions : angle;
            Init(Math.Cos(angle), Math.Sin(angle));
        }

        public static readonly Vector Empty = new Vector(0, 0);

        public static readonly double FullRadions = Math.PI * 2;

        public static Vector operator +(Vector a, Vector b)
        {
            return new Vector(a.X + b.X, a.Y + b.Y);
        }
        public static Vector operator -(Vector a, Vector b)
        {
            return new Vector(a.X - b.X, a.Y - b.Y);
        }

        public static Vector operator -(Vector a)
        {
            return new Vector(-a.X, -a.Y);
        }

        public double ToAngle()
        {
            return Math.Atan2(Y, X);
        }

        public override string ToString()
        {
            return $"X:{X},Y:{Y}";
        }
        public Vector Copy()
        {
            return new Vector(X, Y);
        }
    }

    public interface IPhysicsObject
    {
        void Update(FrameType type);
        void Reset();
    }
}
