﻿using SharpNeat.Genomes.Neat;
using SharpNeat.Phenomes;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Ants
{
    class Ant : GameObject, IBlackBoxContainer
    {
        private int shots = 0;
        public int kills = 0;
        public int deaths = 0;
        public int defends = 0;
        public List<int> visited = new List<int>();
        public int stolen = 0;
        private bool IsHome => CurrentRelmID == ParentRelmID;
        public IBlackBox blackBox { get; set; }

        private double kd => (deaths > 0 ? (kills > 0 ? (kills/deaths) : -deaths) : kills);
        private double eff => kd>=0 ? (kd * 3) - shots : kd*shots;
        public double Fitness => 1000 + stolen + eff + defends;
        //public double Fitness => visited.Count>1?visited.Count:Distance;

        private Vector Centre => new Vector(Relm.centre - Location);
        private Vector Home => new Vector(currentRelm.FindRelm(ParentRelmID, 0) - Location);

        private double Distance => (Relm.centre - Location).Hs / (Relm.Size * Relm.Size) * 4;
        private double Ranking => parentRelm.Background == Brushes.LightGreen ? 1 : parentRelm.Background == Brushes.LightPink ? 0 : 0.5;//winning = 1,losing = 0, otherwise 0.5.
        private double memory;

        private CoOrdinate Target;
        private Vector TargetDirection => Target.Hs > 1 ? new Vector(Target - Location) : new Vector(0, 0);
        private double TargetDistance => Target.Hs > 1 ? (Target - Location).Hs / (Relm.Size * Relm.Size) * 2 : 0;

        private CoOrdinate Dodge;
        private Vector DodgeDirection => Dodge.Hs > 1 ? new Vector(Dodge - Location) : new Vector(0, 0);
        private double DodgeDistance => Dodge.Hs > 1 ? (Dodge - Location).Hs / (Relm.Size * Relm.Size) * 2 : 0;

        const int shootTime = 30;
        int reload = 0;
        bool canShoot => reload > shootTime && currentRelm.pellets.Count < 100;
        double CanShoot => canShoot ? 1 : 0;

        public NeatGenome genome { get; set; }

        public Ant(Relm parentRelm, Brush brush, IBlackBox brain) : base(8, parentRelm)
        {
            blackBox = brain;
            Render = new Ellipse() { Width = 12, Height = 12, Fill = brush, Margin = new Thickness(-6) };
        }

        public override void Update(FrameType type)
        {
            base.Update(type);
            Target = GetEnemy();
            Dodge = GetPellet();
            reload = reload >= shootTime * 3 ? reload : reload + 1 + (RandomAccess.Random.Next(10) == 0 ? 1 : 0);

            //direction += Centre;//circular gravity
            //direction += new Vector(RandomAccess.Random.NextDouble() - .5, RandomAccess.Random.NextDouble() - .5);//random
            //direction += new Vector(Relm.bottom - Location);//linear gravity

            /*
            double[] inputs =
            {
                (Centre.X / 2) + .5,
                (Centre.Y / 2) + .5,
                (Home.X / 2) + .5,
                (Home.Y / 2) + .5,
                Distance,
                (DodgeDirection.X / 2) + .5,
                (DodgeDirection.Y / 2) + .5,
                DodgeDistance,
                memory,
                (TargetDirection.X / 2) + .5,
                (TargetDirection.Y / 2) + .5,
                TargetDistance,
                (Location.X / 2) + .5,
                (Location.Y / 2) + .5,
                IsHome?1:0
            };
            */

            double[] inputs =
            {
                direction.X,
                direction.Y,
                Home.X,
                Home.Y,
                Distance,
                DodgeDirection.X,
                DodgeDirection.Y,
                DodgeDistance,
                memory,
                TargetDirection.X,
                TargetDirection.Y,
                TargetDistance,
                Location.X,
                Location.Y,
                IsHome?1:-1
            };

            blackBox.InputSignalArray.CopyFrom(inputs, 0);
            blackBox.Activate();
            double[] outputs = new double[4];
            blackBox.OutputSignalArray.CopyTo(outputs, 0);
            blackBox.ResetState();

            //direction += new Vector(outputs[0] - .5, outputs[1] - .5);
            //direction += new Vector(outputs[0], outputs[1]);
            if (Math.Abs(outputs[0]) > 0.1)
            {
                double angle = direction.ToAngle() + (outputs[0] / 10);
                direction = new Vector(angle);
            }
            Throttle = Math.Abs(outputs[1]);
            if (outputs[2] > 0.7)
            {
                Shoot();
            }

            memory = outputs[3];
            Steal();
        }

        private void Steal()
        {
            if (!IsHome && RandomAccess.Random.Next(25) == 0)
            {
                parentRelm.Wealth++;
                currentRelm.Wealth--;
                stolen++;
            }
        }

        private CoOrdinate GetPellet()
        {
            CoOrdinate minVal = new CoOrdinate(0, 0);
            double minDist = Relm.Size * Relm.Size;
            for (int i = currentRelm.pellets.Count - 1; i >= 0; i--)
            {
                if (currentRelm.pellets[i].CurrentRelmID != CurrentRelmID)
                {
                    CoOrdinate temp = currentRelm.pellets[i].Location;
                    if ((temp - Location).Hs < minDist)
                    {
                        minVal = temp;
                        minDist = (temp - Location).Hs;
                    }
                }
            }
            return minVal;
        }

        private CoOrdinate GetEnemy()
        {
            CoOrdinate minVal = new CoOrdinate(0, 0);
            double minDist = Relm.Size * Relm.Size;
            for (int i = currentRelm.ants.Count - 1; i >= 0; i--)
            {
                if (currentRelm.ants[i].CurrentRelmID != CurrentRelmID)
                {
                    CoOrdinate temp = currentRelm.ants[i].Location;
                    if ((temp - Location).Hs < minDist)
                    {
                        minVal = temp;
                        minDist = (temp - Location).Hs;
                    }
                }
            }
            return minVal;
        }

        public void AddVisit(int id)
        {
            if (!visited.Contains(id))
            {
                visited.Add(id);
            }
        }

        public void Shoot()
        {
            //if (RandomAccess.Random.NextDouble() >= RandomAccess.Random.NextDouble() + 0.6)
            if (canShoot)
            {
                reload -= shootTime;
                shots++;
                parentRelm.Wealth--;
                Pellet pellet = new Pellet(Location.Copy(), direction.Copy(), parentRelm, this);
                pellet.Throttle = Throttle;
                currentRelm.AddGameObject(Relm.TravelDirection.None, pellet);
            }
        }
    }
}
