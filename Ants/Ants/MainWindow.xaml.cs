﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Windows;
using System.Windows.Media;

namespace Ants
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IPhysicsObject
    {
        private Relm[] Relms => relmSet.Relms;

        private RelmSet relmSet;
        private BrainSet antExperiment;


        Stopwatch stopwatch;
        Timer timer;
        int frameCount = 0;
        bool training = false;
        readonly int trainMult = 1;
        int fm = 1;
        int frameMult { get => fm; set { fm = value > 0 && value <= (trainMult * 5) ? value : fm; } }
        readonly int millis = 40;

        public MainWindow()
        {
            InitializeComponent();
            relmSet = new RelmSet();
            relmSet.RelmsCreated += RelmSet_RelmsCreated;

            antExperiment = new BrainSet(relmSet);

            timer = new Timer(new TimerCallback(Update), null, millis, millis);
            stopwatch = new Stopwatch();
        }

        private void RelmSet_RelmsCreated(object sender, Relm[] e)
        {
            Canvas.Width = Relm.Size * Math.Sqrt(e.Length * 2);
            Canvas.Children.Clear();
            foreach (var item in e)
            {
                Canvas.Children.Add(item);
            }
        }

        public void Update(object o)
        {
            try
            {
                Application.Current.Dispatcher.Invoke(Update);
            }
            catch (Exception)
            {
                // cancel the frame.
            }
        }

        public void Update()
        {
            stopwatch.Start();
            for (int i = 0; i < frameMult; i++)
            {
                frameCount++;
                if (frameCount > 500)
                {
                    frameCount = 0;
                    Reset();
                }
                else if (i == 0)
                {
                    Update(FrameType.animation);
                    int highest = 0;
                    int lowest = 0;
                    for (int w = 0; w < Relms.Length; w++)
                    {
                        Relms[w].Background = Brushes.White;

                        if (Relms[w].Wealth > Relms[highest].Wealth)
                            highest = w;
                        else if (Relms[w].Wealth < Relms[lowest].Wealth)
                            lowest = w;
                    }
                    Relms[highest].Background = Brushes.LightGreen;
                    Relms[lowest].Background = Brushes.LightPink;
                }
                else
                {
                    Update(FrameType.update);
                }
            }
            stopwatch.Stop();

            if (training)
            {
                long elapsed = stopwatch.ElapsedMilliseconds;
                if (elapsed < 0.8 * millis * trainMult)
                {
                    frameMult++;
                }
                else if (elapsed > 0.95 * millis * trainMult)
                {
                    frameMult--;
                }
            }

            stopwatch.Reset();
        }

        public void Update(FrameType type)
        {
            relmSet.Update(type);
        }

        public void Reset()
        {
            antExperiment.Reset();
        }

        private void BtnPreview_Click(object sender, RoutedEventArgs e)
        {
            BtnPreview.IsEnabled = false;
            BtnTrain.IsEnabled = true;
            training = false;
            frameMult = 1;
            timer.Change(millis, millis);
        }

        private void BtnTrain_Click(object sender, RoutedEventArgs e)
        {
            BtnPreview.IsEnabled = true;
            BtnTrain.IsEnabled = false;
            training = true;
            frameMult = trainMult;
            timer.Change(millis * trainMult, millis * trainMult);
        }
    }
}
