﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Ants
{
    class Relm : Canvas, IPhysicsObject
    {
        public enum TravelDirection
        {
            Up,
            Down,
            Left,
            Right,
            Centre,
            None
        }

        public Brush SpeciesBrush;
        public Relm Up, Down, Left, Right;
        public static readonly int Size = 400;
        public List<Pellet> pellets { get; private set; }
        public List<Ant> ants { get; private set; }
        public int ID { get; private set; }
        public static readonly int MaxWealth = 0;
        public static readonly int MinWealth = 0;
        private int relmCount;
        private int maxRelmSearch => 5;
        public double Wealth { get; set; }

        private Label WealthDisplay;

        private readonly Dictionary<int, CoOrdinate> routes = new Dictionary<int, CoOrdinate>();
        private readonly int[] searchPattern = { 0, 1, 2, 3 };
        private int currentSearch = -1;

        public static readonly CoOrdinate top = new CoOrdinate(Size / 2, 0);
        public static readonly CoOrdinate bottom = new CoOrdinate(Size / 2, Size);
        public static readonly CoOrdinate right = new CoOrdinate(Size, Size / 2);
        public static readonly CoOrdinate left = new CoOrdinate(0, Size / 2);
        public static readonly CoOrdinate centre = new CoOrdinate(Size / 2, Size / 2);

        public Relm(int RelmID, int count)
        {
            ID = RelmID;
            relmCount = count;
            pellets = new List<Pellet>();
            ants = new List<Ant>();
            Up = Down = Left = Right = this;
            Wealth = MaxWealth;

            routes.Add(RelmID, centre);

            Rectangle rect = new Rectangle() { Width = Size, Height = Size, Stroke = Brushes.LightSlateGray };
            WealthDisplay = new Label() { Width = Size, Height = Size, FontSize = Size/10 };
            Wealth = Wealth;// used to trigger label text change
            Children.Add(rect);
            Children.Add(WealthDisplay);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="direction">the direction it has gone into</param>
        /// <param name="gameObject"></param>
        public void AddGameObject(TravelDirection direction, GameObject gameObject)
        {
            gameObject.currentRelm.RemoveGameObject(gameObject);
            if (gameObject.GetType() == typeof(Ant))
            {
                ants.Add((Ant)gameObject);
                ((Ant)gameObject).AddVisit(ID);
            }
            else if (direction != TravelDirection.Centre && direction != TravelDirection.None)//implied that the object is a pellet
            {
                //pellets are absorbed by portals
                //Wealth++;
                return;
            }
            else
            {
                pellets.Add((Pellet)gameObject);
            }

            Children.Add(gameObject.Render);
            gameObject.currentRelm = this;
            switch (direction)
            {
                case TravelDirection.Up:
                    gameObject.Location.Y = Size-10;
                    gameObject.Location.X = Math.Max(0, Math.Min(Size, gameObject.Location.X));
                    break;
                case TravelDirection.Down:
                    gameObject.Location.Y = 10;
                    gameObject.Location.X = Math.Max(0, Math.Min(Size, gameObject.Location.X));
                    break;
                case TravelDirection.Left:
                    gameObject.Location.X = Size-10;
                    gameObject.Location.Y = Math.Max(0, Math.Min(Size, gameObject.Location.Y));
                    break;
                case TravelDirection.Right:
                    gameObject.Location.X = 10;
                    gameObject.Location.Y = Math.Max(0, Math.Min(Size, gameObject.Location.Y));
                    break;
                case TravelDirection.Centre:
                    gameObject.Location.X = RandomAccess.Random.Next(50, Size - 50);
                    gameObject.Location.Y = RandomAccess.Random.Next(50, Size - 50);
                    break;
            }
            gameObject.Update(FrameType.spawn);
        }

        public void RemoveGameObject(GameObject gameObject)
        {
            if (gameObject.GetType() == typeof(Ant))
                ants.Remove((Ant)gameObject);
            else
                pellets.Remove((Pellet)gameObject);

            Children.Remove(gameObject.Render);
        }

        public CoOrdinate FindRelm(int id, int iteration)
        {
            CoOrdinate val = null;
            if (currentSearch == -1 && iteration < maxRelmSearch)
            {
                currentSearch = id;

                if (routes.ContainsKey(id))
                {
                    val = routes[id];
                }
                else
                {
                    int[] order = searchPattern.OrderBy(x => RandomAccess.Random.Next()).ToArray();
                    foreach (int item in order)
                    {
                        switch (item)
                        {
                            case 0:
                                val = Right.FindRelm(id, iteration + 1) != null ? left : null;
                                break;
                            case 1:
                                val = Left.FindRelm(id, iteration + 1) != null ? right : null;
                                break;
                            case 2:
                                val = Up.FindRelm(id, iteration + 1) != null ? bottom : null;
                                break;
                            case 3:
                                val = Down.FindRelm(id, iteration + 1) != null ? top : null;
                                break;
                        }
                        if (val != null)
                        {
                            AddRoute(id, val);
                            break;
                        }
                    }
                }

                currentSearch = -1;
            }
            return val;
        }

        private void AddRoute(int id, CoOrdinate value)
        {
            try
            {
                routes.Add(id, value);
            }
            catch (Exception)
            {
                Console.WriteLine("failed");
            }
        }

        public void ClearRoutes()
        {
            routes.Clear();
            routes.Add(ID, centre);
        }

        public void Update(FrameType type)
        {
            for (int i = ants.Count - 1; i >= 0; i--)
            {
                if (i < ants.Count)
                {
                    GameObject gameObject = ants[i];
                    UpdateGameObject(gameObject, type);
                }
            }
            for (int i = pellets.Count - 1; i >= 0; i--)
            {
                GameObject gameObject = pellets[i];
                UpdateGameObject(gameObject, type);
            }
            if (type == FrameType.animation)
            {
                WealthDisplay.Content = Wealth.ToString();
            }
        }

        private void UpdateGameObject(GameObject gameObject, FrameType type)
        {
            gameObject.Update(type);

            if (gameObject.Location.X > Size)
            {
                Left.AddGameObject(TravelDirection.Right, gameObject);
            }
            else if (gameObject.Location.X < 0)
            {
                Right.AddGameObject(TravelDirection.Left, gameObject);
            }
            else if (gameObject.Location.Y > Size)
            {
                Up.AddGameObject(TravelDirection.Down, gameObject);
            }
            else if (gameObject.Location.Y < 0)
            {
                Down.AddGameObject(TravelDirection.Up, gameObject);
            }
        }

        public void Reset()
        {
            for (int i = pellets.Count - 1; i >= 0; i--)
            {
                RemoveGameObject(pellets[i]);
            }
            for (int i = ants.Count - 1; i >= 0; i--)
            {
                RemoveGameObject(ants[i]);
                //ants[i].Reset();
            }
            ants.Clear();
            pellets.Clear();
            Wealth = MaxWealth;
        }
    }
}
