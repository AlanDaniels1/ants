﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Ants
{
    class Pellet : GameObject
    {
        private Ant creator;
        public Pellet(CoOrdinate location, Vector direction, Relm parent, Ant creator) : base(12, parent, location)
        {
            this.creator = creator;
            this.direction = direction;
            Render = new Ellipse() { Width = 6, Height = 6, Fill = Brushes.Black, Margin = new Thickness(-3) };
        }

        public override void Update(FrameType type)
        {
            base.Update(type);

            for (int i = currentRelm.ants.Count - 1; i >= 0; i--)
            {
                if (i < currentRelm.ants.Count)
                {
                    if (currentRelm.ants[i].ParentRelmID != ParentRelmID && (currentRelm.ants[i].Location - Location).Hs <= 81)//9 ^ 2 == 81
                    {
                        RegisterShot(currentRelm.ants[i]);
                    }
                }
            }
        }

        public void RegisterShot(Ant ant)
        {
            //currentRelm.Wealth += 6;
            //parentRelm.Wealth += 5;
            //ant.parentRelm.Wealth -= 10;
            parentRelm.Wealth += 4;
            ant.parentRelm.Wealth -= 3;

            if (parentRelm == currentRelm)
            {
                parentRelm.Wealth += 3;
                creator.defends++;
            }

            int stolen = Math.Max(ant.stolen / 4,0);
            ant.stolen -= stolen;
            creator.stolen += stolen;

            creator.kills++;
            ant.deaths++;

            ant.Reset();
            currentRelm.RemoveGameObject(this);
        }

        private double GetWealth(Ant ant, double v)
        {
            double wealth = ant.parentRelm.Wealth;
            if (wealth < v)
            {
                ant.parentRelm.Wealth = 0;
                return wealth;
            }
            else
            {
                ant.parentRelm.Wealth -= v;
                return v;
            }
        }
    }
}
