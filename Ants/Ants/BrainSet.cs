﻿using SharpNeat.Core;
using SharpNeat.EvolutionAlgorithms;
using SharpNeat.Genomes.Neat;
using SharpNeat.Phenomes;
using System;
using System.Collections.Generic;
using System.Windows.Media;
using System.Xml;

namespace Ants
{
    class BrainSet : IPhysicsObject
    {
        private Relm[] Relms => relmSet.Relms;
        private RelmSet relmSet;
        private int population => evolutionAlgorithm.GenomeList.Count;
        private int speciesCount=>evolutionAlgorithm.SpecieList.Count;
        private uint currentGeneration => evolutionAlgorithm.CurrentGeneration;
        private List<Ant> ants;

        DependantNeatEvolutionAlgorithm<NeatGenome, Ant> evolutionAlgorithm;
        AntExperiment antExperiment;
        IGenomeDecoder<NeatGenome,IBlackBox> genomeDecoder;

        private NeatGenomeFactory genomeFactory = new NeatGenomeFactory(11, 4);

        public BrainSet(RelmSet relmSet)
        {
            this.relmSet = relmSet;
            ants = new List<Ant>();
            antExperiment = new AntExperiment();
            var doc = new XmlDocument();
            doc.Load("AntExperimentConfig.xml");
            antExperiment.Initialize("ant experiment", doc.DocumentElement);

            genomeDecoder = antExperiment.CreateGenomeDecoder();

            evolutionAlgorithm = (DependantNeatEvolutionAlgorithm<NeatGenome, Ant>)antExperiment.CreateEvolutionAlgorithm();
            evolutionAlgorithm.NewGenomeCreated += EvolutionAlgorithm_NewGenomeCreated;

            relmSet.GenarateRelms(speciesCount);

            for (uint i = 0; i < relmSet.Count; i++)
            {
                Brush brush = new SolidColorBrush(new Color() { A = 255, R = (byte)RandomAccess.Random.Next(150, 200), G = (byte)RandomAccess.Random.Next(50, 200), B = (byte)RandomAccess.Random.Next(50, 200) });
                Relms[i].SpeciesBrush = brush;
            }
            evolutionAlgorithm.Reset(ants.ToArray());
        }

        private void EvolutionAlgorithm_NewGenomeCreated(object sender, NeatGenome e)
        {
            IBlackBox blackBox = genomeDecoder.Decode(e);
            int id = e.SpecieIdx;
            Ant ant = new Ant(Relms[id], Relms[id].SpeciesBrush, blackBox) {genome = e };
            relmSet.AddAnt(id, ant);
            ants.Add(ant);
        }

        public void Reset()
        {
            relmSet.Reset();
            Ant[] antSet = ants.ToArray();
            ants.Clear();
            evolutionAlgorithm.Reset(antSet);
        }

        public void Update(FrameType type)
        {
            relmSet.Update(type);
        }
    }
}
