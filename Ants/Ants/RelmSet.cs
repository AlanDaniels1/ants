﻿using System;

namespace Ants
{
    class RelmSet : IPhysicsObject
    {
        public Relm[] Relms { get; private set; }
        public int Count => Relms.Length;
        public event EventHandler<Relm[]> RelmsCreated;

        public RelmSet() { }

        public void GenarateRelms(int size)
        {
            Relms = new Relm[size];
            for (int i = 0; i < size; i++)
            {
                Relms[i] = new Relm(i, Relms.Length) { Width = Relm.Size, Height = Relm.Size };
            }
            ShuffleGates();
            RelmsCreated?.Invoke(this, Relms);
        }

        public void AddAnt(int relmID, Ant ant)
        {
            Relms[relmID].AddGameObject(Relm.TravelDirection.Centre, ant);
        }

        private void ShuffleGates()
        {
            Relm[] tops, bottoms, lefts, rights;
            tops = (Relm[])Relms.Clone();
            bottoms = (Relm[])Relms.Clone();
            lefts = (Relm[])Relms.Clone();
            rights = (Relm[])Relms.Clone();

            Shuffle(ref tops);
            Shuffle(ref bottoms);
            Shuffle(ref lefts);
            Shuffle(ref rights);


            for (int i = 0; i < Relms.Length; i++)
            {
                Relms[i].Up = tops[i];
                Relms[i].Down = bottoms[i];
                Relms[i].Left = lefts[i];
                Relms[i].Right = rights[i];
                Relms[i].ClearRoutes();
            }
        }

        private void Shuffle<T>(ref T[] input)
        {
            for (int i = 0; i < input.Length; i++)
            {
                Swap(ref input, i, i + RandomAccess.Random.Next(input.Length - i));
            }
        }

        private void Swap<T>(ref T[] input, int a, int b)
        {
            T temp = input[a];
            input[a] = input[b];
            input[b] = temp;
        }

        public void Update(FrameType type)
        {
            foreach (Relm relm in Relms)
            {
                relm.Update(type);
            }
        }

        public void Reset()
        {
            foreach (Relm relm in Relms)
            {
                relm.Reset();
            }
            ShuffleGates();
        }
    }
}
